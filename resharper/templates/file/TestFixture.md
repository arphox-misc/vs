```cs
using NUnit.Framework; 

namespace $NAMESPACE$ 
{ 
    /// <summary> 
    ///     Unit tests for <see cref="$TestedClassName$"/> 
    /// </summary> 
    [TestFixture] 
    public sealed class $CLASS$ 
    { 
        $END$ 
    } 
} 
```

https://gist.github.com/arphox/f1dfc79048548508982fcc276584a5ec
