```cs
[Test] 
public void If$ParameterName$IsNull_ThrowsException() 
{ 
    // Arrange & Act 
    Action act = () => $END$;

    // Assert 
    act.Should().Throw<ArgumentNullException>() 
        .WithMessage("*$ParameterName$*"); 
} 
```

**Also uses FluentAssertions!**

https://gist.github.com/arphox/9f37fa5c88851b779539a65c5abac2cd
