Extensions that I use:
- [Add New File](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.AddNewFile) ([VS22](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.AddNewFile64)) - easily adding new files to any project. Simply hit Shift+F2.
- [Editor Guidelines](https://marketplace.visualstudio.com/items?itemName=PaulHarrington.EditorGuidelines) ([VS22](https://marketplace.visualstudio.com/items?itemName=PaulHarrington.EditorGuidelinesPreview)) - adds vertical column guides behind your code 
- [MiddleClickDefinition](https://marketplace.visualstudio.com/items?itemName=norachuga.MiddleClickDefinition) ([VS22](https://marketplace.visualstudio.com/items?itemName=norachuga.MiddleClickDefinition64)) - Use middleclicks to trigger Go To Definition, Go To Implementation, or Peek Definition. Because VS can't do it by default, shame on them.
- [Trailing Whitespace Visualizer](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.TrailingWhitespaceVisualizer) - Keeps your code files clean by making it easier than ever to identify and remove any trailing whitespace
- [VSColorOutput64](https://marketplace.visualstudio.com/items?itemName=MikeWard-AnnArbor.VSColorOutput64) - colors output and can show a build time summary. Though colors are ugly so I use some colors from the "Output enhancer" extension.
- [Copy Git Link 2022](https://marketplace.visualstudio.com/items?itemName=EtienneBAUDOUX.CopyGitLink2022&ssr=false#overview) - in another similar extension they stated their feature set is already built in in VS22 but I couldn't find it.
- [Roslynator (VS22)](https://marketplace.visualstudio.com/items?itemName=josefpihrt.Roslynator2022)

May not be necessary anymore:
- [Mouse Navigation](https://marketplace.visualstudio.com/items?itemName=SamHarwell.MouseNavigation) - mouse buttons to code Navigation, not sure if needed by VS22

Sounds good:
- [Code Cleanup On Save](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.CodeCleanupOnSave) - Automatically run one of the Code Clean profiles when saving a document
- [(VS19 only) Learn the Shortcut](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.LearntheShortcut) - only for **VS 19** :()

Alternatives:
- [Output enhancer](https://marketplace.visualstudio.com/items?itemName=NikolayBalakin.Outputenhancer) - colors output and has looks nicer than VSColorOutput64, but can't show build time summary and similar stuff.

By the way, [Mads Kristensen](https://marketplace.visualstudio.com/publishers/MadsKristensen) makes great extensions!
