# Shortcuts

## Intro
Shortcuts are one of the more important productivity tools, and there is a big mess in terms of defaults and shortcuts.  
Why? VS19 has the **Default**, the **Visual C# 2005** and the **ReSharper (Visual Studio)** mapping schemes. Resharper also has at least 2: **Visual Studio** and **IJ IDEA/ReSharper 2.x**. There is also Rider, which also has its own mapping schemes.  
Truth is, there are a lot of overlaps, but unfortunately there is no "best one".  
So here I go, I present my own "best" and unified keyboard mapping scheme which aims to be unambigous, easy to learn and multipurpose (takes Resharper and Rider into account, too).

I carefully analyzed all the shortcuts below; their default shorcuts in most schemes, and chose that is the most logical one for me.

Here are the defaults for:
- [VS](https://docs.microsoft.com/en-us/visualstudio/ide/default-keyboard-shortcuts-in-visual-studio?view=vs-2019)
- [ReSharper](https://www.jetbrains.com/help/resharper/Reference__Keyboard_Shortcuts.html)
- [Rider](https://www.jetbrains.com/help/rider/Reference_Keyboard_Shortcuts_Index.html)

Keep in mind that Rider's:
- **ReSharper/ ReSharper (macOS)** scheme should be very similar to ReSharper's **ReSharper 2.x/IJ IDEA** scheme and
- **Visual Studio/ Visual Studio (macOS)** scheme should be very similar to ReSharper's **Visual Studio** scheme and thus to _one of_ VS's default schemes.

See this useful page:  
[Keyboard shortcuts: keymaps comparison (Windows)](https://www.jetbrains.com/help/rider/Keymaps_Comparison_Windows.html)

The order or command keys in the reference are: `Ctrl` -> `Shift` -> `Alt` to follow the conventions of VS's keyboard mapping scheme editor.  
There are collisions for different IDE shortcuts, because they simply cannot co-exist together (i.e. VS and Rider).  
All shortcuts are Global shortcuts, except where it is explicitly stated.  
Icons:
- ❌: not exists, and I miss it
- ✖: not exists, but it's okay (e.g. for RD shortcuts where VS already has the same feature)
- ⚠: parts of the feature exist
- ❔: maybe exists, but I haven't found it
- ??? : I don't know, I haven't checked

A general recommendation for VS+RS combo is when the same functionality is available in VS and RS, I prefer the VS ones to reduce dependency on RS (and so it will work while RS is disabled), and hopefully VS's one is faster due it is built-in, and not provided through an extension.

## Coding assistance
Category name is from [Rider/Resharper](https://www.jetbrains.com/help/rider/Reference_Keyboard_Shortcuts_Index.html#coding_assistance).
| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| Generate | VS: ⚠<br/>RD: [Generate Type Members](https://www.jetbrains.com/help/rider/Generating_Type_Members.html)<br/>RS: [ReSharper_Generate](https://www.jetbrains.com/help/resharper/Code_Generation__Index.html) | `Alt+Insert` | The RS/RD feature can be used for a lot of tasks: e.g. generating code in the code editor (ctors, props, members, patterns, etc) and new files/dirs in the solution explorer.<br/>The `ReSharper_GenerateNewFolder` action instantly creates a new folder in the solution explorer, if you need that. <br/>In VS, `File.NewFile` can also help for file generation in the solution explorer.<br/>Shortcut source: RS, RD, IJ
| Move Code Down | VS: ❌<br/>RD: [Move Statement Down](https://www.jetbrains.com/help/rider/Coding_Assistance__Moving_Code_Elements.html)<br/>RS: [ReSharper_MoveDown](https://www.jetbrains.com/help/resharper/Coding_Assistance__Moving_Code_Elements.html) | `Ctrl+Shift+Alt+Down` | RD: Although in Rider it is called Move **Statement**, AFAIK it's same than ReSharper's action.<br/>Shortcut source: RS, RD. |
| Move Code Up | VS: ❌<br/>RD: [Move Statement Up](https://www.jetbrains.com/help/rider/Coding_Assistance__Moving_Code_Elements.html)<br/>RS: [ReSharper_MoveUp](https://www.jetbrains.com/help/resharper/Coding_Assistance__Moving_Code_Elements.html) | `Ctrl+Shift+Alt+Up` | RD: Although in Rider it is called Move **Statement**, AFAIK it's same than ReSharper's action.<br/>Shortcut source: RS, RD. |

## Basic editor actions
Basic text manipulation.
| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| Move Line Down | VS: Edit.MoveSelectedLinesDown<br/>RD: [Move Line Down](https://www.jetbrains.com/help/rider/Adding_Deleting_and_Moving_Lines.html)<br/>RS: ✖ | `Alt+Down` | Shortcut source: VS, VSCode. |
| Move Line Up | VS: Edit.MoveSelectedLinesUp<br/>RD: [Move Line Up](https://www.jetbrains.com/help/rider/Adding_Deleting_and_Moving_Lines.html)<br/>RS: ✖ | `Alt+Up` | Shortcut source: VS, VSCode. |
| Duplicate Selection | VS: Edit.Duplicate<br/>RD: [Duplicate Line or Selection](https://www.jetbrains.com/help/rider/Coding_Assistance__Duplicate_Line_Block.html)<br/>(RS: [ReSharper_DuplicateText](https://www.jetbrains.com/help/resharper/Coding_Assistance__Duplicate_Line_Block.html)) | `Ctrl+D` | VS: I use VS as I think RS's one is the same. <br/>Shortcut source: VS, RS, IJ |
| Delete line | VS: Edit.LineDelete<br/>RD: ✖<br/>RS: ??? | `Shift+Del` | Shortcut source: myself? | 
| CharLeftExtendColumn | VS: Edit.CharLeftExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+Left` | Shortcut source: VS | 
| CharRightExtendColumn | VS: Edit.CharRightExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+Right` | Shortcut source: VS | 
| LineUpExtendColumn | VS: Edit.LineUpExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+Up` | Shortcut source: VS | 
| LineDownExtendColumn | VS: Edit.LineDownExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+Down` | Shortcut source: VS | 
| LineStartExtendColumn | VS: Edit.LineStartExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+Home` | Shortcut source: VS | 
| LineEndExtendColumn | VS: Edit.LineEndExtendColumn<br/>RD: ✖<br/>RS: ??? | `Shift+Alt+End` | Shortcut source: VS | 

## Build, Run, Debug
Category name is from [Rider/Resharper](https://www.jetbrains.com/help/rider/Reference_Keyboard_Shortcuts_Index.html#coding_assistance).
| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| Build Solution | VS: Build.BuildSolution<br/>RD: [Build Solution](https://www.jetbrains.com/help/rider/Building_Projects.html)<br/>RS: ❔ | `F6` | Because who on Earth would want to press 3 buttons to build a solution?<br/>Shortcut source: VS (Visual C# 2005 scheme) |
| Add Quickwatch | VS: Debug.QuickWatch<br/>RD: ??? <br/>RS: ??? | `Ctrl+Alt+Q` | Shortcut source: myself |
| Add Watch | VS: Debug.AddWatch<br/>RD: ??? <br/>RS: ??? | `Ctrl+Alt+W` | Shortcut source: Ctrl+Alt+W, 1/2/3/4 was already a hotkey for switching to a given Watch window, I removed them and have it as "add watch". |

## Refactor
Since refactor shortcuts are used very often, they should be easily reachable, so they shouldn't be a combo (e.g. `Ctrl+R, Ctrl+M`) , and this means VS's `Ctrl+R, Ctrl+<whatever>` logic is a no go.

| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| Extract Method<br/>(Extract Local Function) | VS: Refactor.ExtractMethod<br/>RD: [Extract Method](https://www.jetbrains.com/help/rider/Refactorings__Extract_Method.html)<br/>RS: [ReSharper_ExtractMethod](https://www.jetbrains.com/help/resharper/Refactorings__Extract_Method.html) | `Ctrl+Alt+M` | If RS is enabled, VS's action cannot be used, RS overrides it. In RS, it also allows to Extract as a Local Function. <br/>Shortcut source: IJ. |
| Inline (variable, method, etc.) | VS: ❌<br/>RD: [Inline](https://www.jetbrains.com/help/rider/Inline_refactorings.html)<br/>RS: [ReSharper_InlineVariable](https://www.jetbrains.com/help/resharper/Inline_refactorings.html) | `Ctrl+Alt+N` | VS can has very limited inlining abilities in the lightbulb menu. <br/>Shortcut source: IJ. |
| Introduce Variable | VS: ❌<br/>RD: [Introduce Variable](https://www.jetbrains.com/help/rider/Refactorings__Introduce_Variable.html)<br/> RS: [ReSharper_IntroVariable](https://www.jetbrains.com/help/resharper/Refactorings__Introduce_Variable.html) | `Ctrl+Alt+V` | VS has this in the lightbulb menu. <br/>Shortcut source: IJ. |
| Refactor This | VS: ❌<br/>RD: [Refactor This](https://www.jetbrains.com/help/rider/Refactor_This.html)<br/>RS: [ReSharper_RefactorThis](https://www.jetbrains.com/help/resharper/Refactor_This.html) | `Ctrl+Shift+R` | Shortcut source: RS. |
| Rename | VS: Refactor.Rename<br/>RD: [Rename](https://www.jetbrains.com/help/rider/Refactorings__Rename.html)<br/>RS: [ReSharper_Rename](https://www.jetbrains.com/help/resharper/Refactorings__Rename.html)| `F2` | If RS is enabled, VS's action cannot be used, RS overrides it.<br/>RD/RS can rename a lot of things with this shortcut.<br/>Shortcut source: VS (Visual C# 2005 scheme), RS |

## Window
| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| Keep Tab Open<br/> | VS: Window.KeepTabOpen<br/>RD: ✖<br/>RS: ✖ | `Ctrl+Alt+Home` | Only relevant to VS since it has preview tabs. Brings a preview tab to a permanent tab. <br/>Shortcut source: VS. |
| Pin Tab <br/> | VS: Window.PinTab<br/>RD: Pin Active Tab<br/> RS: ✖ | `Ctrl+Alt+End` | Shortcut source: myself |
| Sync with active document<br/> | VS: SolutionExplorer.SyncWithActiveDocument<br/>RD: [Locate/Select in Solution View](https://www.jetbrains.com/help/rider/Navigation_and_Search__Context_Dependent_Navigation.html#locating-current-document-in-explorer)<br/>RS: ✖ | `Shift+Alt+S` | Shortcut source: Older version of VS, RD (VS+RS) |
| Show Solution Explorer | VS: View.SolutionExplorer<br/>RD: ✖<br/>RS: ??? | `Ctrl+NumPad 1` | Shortcut source: myself |
| Show Git Changes | VS: View.GitWindow<br/>RD: ???<br/>RS: ??? | `Ctrl+NumPad 2` | Shortcut source: myself |
| Show Test Explorer | VS: TestExplorer.ShowTestExplorer<br/>RD: ???<br/>RS: ??? | `Ctrl+NumPad 3` | Shortcut source: myself |

## "Solution Explorer"-scoped
| Action | Aliases | Shortcut | Notes |
| ------ | ------- | -------- | ----- |
| New folder | VS: Project.NewFolder<br/>RD: ???<br/>RS: ??? | `F7` | Shortcut source: myself |
